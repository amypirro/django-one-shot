from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todolist_list(request):
    lists = TodoList.objects.all()
    context = {
        "todolist_list": lists,
    }
    return render(request, "todos/list.html", context)


def todolist_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "todolist": list,
    }
    return render(request, "todos/detail.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)

    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def edit_todolist(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            form = form.save()
            return redirect("todo_list_detail", id=form.id)

    else:
        form = TodoListForm(instance=list)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


def create_todoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)

    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "todos/create_item.html", context)


##### you're editing this
def edit_todoitem(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", item.list.id)

    else:
        form = TodoItemForm(instance=item)

    context = {"form": form}

    return render(request, "todos/edit_item.html", context)


def delete_todolist(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")
