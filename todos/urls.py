from django.urls import path
from todos.views import (
    todolist_list,
    todolist_detail,
    create_todolist,
    edit_todolist,
    delete_todolist,
    create_todoitem,
    edit_todoitem,
)

urlpatterns = [
    path("", todolist_list, name="todo_list_list"),
    path("<int:id>/", todolist_detail, name="todo_list_detail"),
    path("create/", create_todolist, name="todo_list_create"),
    path("<int:id>/edit/", edit_todolist, name="todo_list_update"),
    path("<int:id>/delete/", delete_todolist, name="todo_list_delete"),
    path("items/create/", create_todoitem, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_todoitem, name="todo_item_update"),
]
